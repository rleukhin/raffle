<?php

declare(strict_types = 1);

Route::get('csv-generate', [
    'as'         => '.csv_generate',
    'uses'       => 'EventActionController@csvGenerate',
    'middleware' => ['guest'],
]);

Route::get('event_action', [
    'as'         => '.event_action',
    'uses'       => 'EventActionController@eventAction',
    'middleware' => ['guest'],
]);
//admin/event_action

Route::post('create-event', [
    'as'         => '.create_event',
    'uses'       => 'EventActionController@createEvent',
    'middleware' => ['guest'],
]);

Route::post('update-event', [
    'as'         => '.update_event',
    'uses'       => 'EventActionController@updateEvent',
    'middleware' => ['guest'],
]);
