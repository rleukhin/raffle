<?php

declare(strict_types = 1);

Route::group(['prefix' => 'event', 'as' => '.events'], __DIR__.'/client/events.php');
