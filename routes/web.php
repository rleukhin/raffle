<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(
    [
        'namespace'  => 'Admin',
        'as'         => 'admin',
        'prefix'     => 'admin',
        'middleware' => 'guest',
    ],
    __DIR__.'/admin.php'
);

Route::group(
    ['namespace' => 'Client', 'as' => 'client'],
    __DIR__.'/client.php'
);

Route::post('/generate-pdf', 'PDFController@generatePDF')->name('generate_pdf');
