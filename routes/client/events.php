<?php

Route::get('/', [
    'as'         => '.event_page',
    'uses'       => 'EventController@eventPage',
    'middleware' => ['guest'],
]);

Route::post('save-new-user', [
    'as'         => '.save_new_user',
    'uses'       => 'EventController@saveNewUser',
    'middleware' => ['guest'],
]);
