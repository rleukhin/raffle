composer dumpautoload

php artisan down

npm run dev

php artisan migrate
php artisan cache:clear
php artisan view:clear
php artisan queue:restart
php artisan config:clear
php artisan route:clear
php artisan laroute:generate

composer dump-autoload

php artisan up
