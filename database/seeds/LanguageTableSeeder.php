<?php

use Illuminate\Database\Seeder;
use App\Language;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Language::query()->insert(
            [
                'lang_name' => 'German',
                'default_lang' => 1,
            ]
        );
        Language::query()->insert(
            [
                'lang_name' => 'English',
            ]
        );
    }
}
