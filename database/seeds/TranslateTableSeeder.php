<?php

use Illuminate\Database\Seeder;
use App\Translate;

class TranslateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $array = array(
            'German' => array(
                'lang_name' => 'German',
                'first_name' => 'Vorname',
                'last_name' => 'Nachname',
                'participate_in_raffle' => 'an Gewinnspiel teilnehmen',
                'signup_for_newsletter' => 'Newsletteranmeldung',
                'please_sign_here_with_finger' => 'Bitte hier mit Finger unterschreiben',
                'send' => 'Abschicken',
                'you_already_signed_up' => 'Sie haben sich schon registriert',
                'thank_you_for_participating' => 'Vielen Dank! Wir wünschen Ihnen einen angenehmen Aufenthalt!',
                'clear' => 'Klar',
            ),
            'English' => array(
                'lang_name' => 'English',
                'first_name' => 'First name',
                'last_name' => 'Last Name',
                'participate_in_raffle' => 'Participate in raffle',
                'signup_for_newsletter' => 'Signup for newsletter',
                'please_sign_here_with_finger' => 'Please sign here with finger',
                'send' => 'Send',
                'you_already_signed_up' => 'You already signed up',
                'thank_you_for_participating' => 'Thank you for participating! Enjoy the rest of the show!',
                'clear' => 'Clear',
            ),
        );
        foreach($array as $lang => $translates) {
            if ($lang == 'German') {
                $i = 1;
            } else {
                $i = 2;
            }
            foreach($translates as $key => $val) {
                Translate::query()->insert(
                    [
                        'lang_id' => $i,
                        'key' => $key,
                        'value' => $val,
                    ]
                );
            }
        }
    }
}


// class LanguageTableSeeder extends Seeder
// {
//     /**
//      * Run the database seeds.
//      *
//      * @return void
//      */
//     public function run()
//     {
//         $array = array(
//             'German' => array(
//                 'lang_name' => 'German',
//                 'default_lang' => 1,
//                 'first_name' => 'Vorname',
//                 'last_name' => 'Nachname',
//                 'participate_in_raffle' => 'an Gewinnspiel teilnehmen',
//                 'signup_for_newsletter' => 'Newsletteranmeldung',
//                 'please_sign_here_with_finger' => 'Bitte hier mit Finger unterschreiben',
//                 'send' => 'Abschicken',
//                 'you_already_signed_up' => 'Sie haben sich schon registriert',
//                 'thank_you_for_participating' => 'Vielen Dank! Wir wünschen Ihnen einen angenehmen Aufenthalt!',
//             ),
//             'English' => array(
//                 'lang_name' => 'English',
//                 'default_lang' => 0,
//                 'first_name' => 'First name',
//                 'last_name' => 'Last Name',
//                 'participate_in_raffle' => 'Participate in raffle',
//                 'signup_for_newsletter' => 'Signup for newsletter',
//                 'please_sign_here_with_finger' => 'Please sign here with finger',
//                 'send' => 'Send',
//                 'you_already_signed_up' => 'You already signed up',
//                 'thank_you_for_participating' => 'Thank you for participating! Enjoy the rest of the show!',
//             ),
//         );
//         foreach($array as $lang => $val) {
//             Language::query()->insert(
//                 [
//                     'lang_name' => $val['lang_name'],
//                     'default_lang' => $val['default_lang'],
//                     'first_name' => $val['first_name'],
//                     'last_name' => $val['last_name'],
//                     'participate_in_raffle' => $val['participate_in_raffle'],
//                     'signup_for_newsletter' => $val['signup_for_newsletter'],
//                     'please_sign_here_with_finger' => $val['please_sign_here_with_finger'],
//                     'send' => $val['send'],
//                     'you_already_signed_up' => $val['you_already_signed_up'],
//                     'thank_you_for_participating' => $val['thank_you_for_participating'],
//                 ]
//             );
//         }

//     }
// }
