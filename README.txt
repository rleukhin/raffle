Pull project from branch

Input all bash command from project directory

composer update
composer install

Create Database

Create .env file for example .env.example

Input all neccessary data to .env file( database name and credentiall, application url and domain etc)

Generate key for your application
    php artisan key:generate

Clear the old boostrap/cache/compiled.php
	php artisan clear-compiled

Recreate boostrap/cache/compiled.php
	php artisan optimize

Migrate any database changes
	php artisan migrate:refresh --seed

To fill tables
   php artisan db:seed

generate routes for js
   php artisan laroute:generate

npm install

npm run dev

Run: /bin/deploy.sh

