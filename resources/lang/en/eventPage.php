<?php

return [

    'lang_name' => 'English',
    'default_lang' => 0,
    'first_name' => 'First name',
    'last_name' => 'Last Name',
    'participate_in_raffle' => 'Participate in raffle',
    'signup_for_newsletter' => 'Signup for newsletter',
    'please_sign_here_with_finger' => 'Please sign here with finger',
    'send' => 'Send',
    'you_already_signed_up' => 'You already signed up',
    'thank_you_for_participating' => 'Thank you for participating! Enjoy the rest of the show!',

];
