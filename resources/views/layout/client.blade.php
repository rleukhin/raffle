<!DOCTYPE html>
<html lang="{{ Lang::locale() }}">
<head>
    <title>
            @yield('title')
        </title>
        <!--<meta name="csrf-token" content="{ csrf_token() }}">-->
        <meta http-equiv="X-UA-Compatible" content="IE=11">
        <meta name="theme-color" content="#1e8bbb">
        @yield('meta_description')
        <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="/js/laroute.js"></script>
</head>
<body style="background-color: #EBEBEB;">
    <div id="app">
        @include('client.partial.header')
        @yield('content')
        @include('client.partial.footer')
    </div>
</body>
<script src="{{ asset('js/app.js') }}"></script>
</html>
