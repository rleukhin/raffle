@extends('layout/client')

@section('content')
    <event-action :events="{{ $events }}"></event-action>
@endsection
