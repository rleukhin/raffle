@extends('layout/client')

@section('content')
<event-page :langs="{{ $langs  }}" :event="{{ $event }}"></event-page>
@endsection
