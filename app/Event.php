<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    public function language()
    {
        return $this->hasOne('App\Language', 'id', 'lang_id');
    }

    public function event_user()
    {
        return $this->hasMany('App\EventUser', 'event_id', 'id');
    }
}
