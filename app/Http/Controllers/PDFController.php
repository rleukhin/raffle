<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PDFController extends Controller
{
    //
    public function generatePDF($request)
    {
        $pdf = PDF::loadView('PDFDom/PDFDom', ['data' => $request['userSignature']]);
        $pdf->save(database_path().'/PDFsignatures/' . $request['e_mail'] . '.pdf');

    }
}
