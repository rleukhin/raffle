<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Event;
use App\Http\Controllers\PDFController;
use App\EventUser;
use App\Language;
use App\Translate;

class EventController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function eventPage()
    {
        $eventID = $_GET['id'];
        $event = Event::query()->where('id', $eventID)->first();
        $lang = Language::query()->where('id', $event['langs_id'])->first();
        $translateArray = Translate::query()->where('lang_id', $lang['id'])->get();
        $newTranslateArray = [];
        foreach($translateArray as $val) {
            $newTranslateArray[$val['key']] = $val;
        }
        if ($event == null) {
            return View::make('client/event_not_found');
        }
        return View::make('client/event_page', ['langs' => json_encode($newTranslateArray), 'event' => $event]);
    }

    public function saveNewUser(Request $request)
    {
        $newUser = $request->all()['newUser'];
        app(PDFController::class)->generatePDF($newUser);
        EventUser::query()->insertGetId(
            [
                'event_id' => $newUser['event_id'],
                'language_id' => 0,
                'first_name' =>  $newUser['firstname'],
                'last_name' => $newUser['lastname'],
                'email' => $newUser['e_mail'],
                'participate_in_raffle' => $newUser['raffle'],
                'signup_for_newsletter' => $newUser['newsletter'],
                'path_to_signature' => './database/PDFsignatures/' . $newUser['e_mail'] . '.pdf']
        );
        $signups = (int) EventUser::query()->where('event_id', $newUser['event_id'])->count();
        Event::query()
        ->where('id', $newUser['event_id'])->update(
            [
                'signups' => $signups,
            ]
        );
    }
}
