<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use App\Event;
use App\EventUser;
use View;

class EventActionController extends Controller
{
    //
    public function eventAction()
    {
        return View::make('admin/event_action', ['events' => Event::query()->orderBy('id', 'desc')->get()]);
    }

    public function createEvent(Request $request)
    {
        $newEvent = $request->all()['eventData'];
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https' ? 'https' : 'http';
        $lastID = 1 + (int) Event::query()->orderBy('id', 'desc')->first()['id'];
        Event::query()->insertGetId(
            [
                'csv_file_id' => 0,
                'langs_id' =>  $newEvent['lang_id'],
                'event_name' => $newEvent['eventName'],
                'raffle' => $newEvent['raffle'],
                'newsletter' => $newEvent['newsletter'],
                'event_link' => $protocol . '://' . $_SERVER['SERVER_NAME'] . '/event' . '?id=' . $lastID,
                'signups' => 0]
        );
    }

    public function updateEvent(Request $request)
    {
        $event = $request->all()['eventData'];
        Event::query()
        ->where('id', '=', $event['id'])->update(
            [
                'event_name' => $event['event_name'],
                'raffle' => $event['raffle'],
                'newsletter' => $event['newsletter'],
            ]
        );
    }

    public function csvGenerate(Request $request)
    {
        $eventID = $request->all()['eventData'];
        $eventUsers = EventUser::query()
        ->where('event_id', $eventID)
        ->join('events', 'events.id', '=', 'event_users.event_id')
        ->get();
        $flow = fopen('csvfile.csv', 'w+');
        fputcsv($flow, array('event_name', 'raffle', 'newsletter', 'first_name', 'last_name', 'e-mail', 'signature_path'));
        foreach($eventUsers as $row) {
            fputcsv(
                $flow, array($row['event_name'], $row['participate_in_raffle'], $row['signup_for_newsletter'],
                $row['first_name'], $row['last_name'], $row['email'],
                './database/PDFsignatures/' . $row['email'] . '.pdf'));
        }
        fclose($flow);
        $headers = array(
            'Content-Type' => 'text/csv',
        );

        return Response::download('csvfile.csv', 'EventID--'. $eventID . '.csv', $headers);
    }
}
